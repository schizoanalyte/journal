---
layout: post
title:  "curtis yarvin (mencius moldbug) recent 3 hour live at the based deleuze party in LA"
date:   2020-03-28 15:33:14 +0200
---


> *"..i'm not telling you the nazis aren't on crack, the nazis are definitely on crack. the soviets are also on crack. and sort of the true history of these events is so dark, and so useless, and so grim and so weird, that nobody wants it. and so, in a way, if you're basically looking for useful history, you're not gonna find that reality — and yet in some ways that reality is actually more useful than these, like, sort of [...] stories."*
>
> *"i've warned people earlier that the thing that's really important not to do is basically inhabit their caricature of you — and that's exactly what you're doing if you're a nazi or a white nationalist."*
>
> ​        — curtis yarvin (a.k.a. mencius moldbug) [live at the based deleuze release party in LA from march 19, 2020](https://www.youtube.com/watch?v=RRQO3VbJsMw)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/%D0%9F%D1%80%D0%B8%D1%82%D1%87%D0%B0_%D0%BE_%D1%81%D0%BB%D0%B5%D0%BF%D1%8B%D1%85.jpeg/1280px-%D0%9F%D1%80%D0%B8%D1%82%D1%87%D0%B0_%D0%BE_%D1%81%D0%BB%D0%B5%D0%BF%D1%8B%D1%85.jpeg)

i've only fairly recently discovered the writings of [mencius moldbug](https://www.unqualified-reservations.org/) (the pen name of [curtis yarvin](https://en.wikipedia.org/wiki/Curtis_Yarvin), a computer programmer also associated with the founding of the [urbit](https://urbit.org/) project in 2002) — a [collection](https://www.unqualified-reservations.org) of political essays, investigations, diagnoses, rants and a few theoretical propositions, the [corpus](https://www.unqualified-reservations.org) of which is considered to be the foundation of what developed into the [neo-reactionary movement](https://rationalwiki.org/wiki/Neoreactionary_movement) and later the [dark enlightenment](https://en.wikipedia.org/wiki/Dark_Enlightenment) ([nick land](https://en.wikipedia.org/wiki/Nick_Land)'s term for the same).


