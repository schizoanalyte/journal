---
layout: post
title:  "deterritorialization"
date:   2020-03-28 16:41:14 +0200

---

the triad of **deterritorialization**, **reterritorialization**, and **territory** is among the more frequently  encountered deleuzian concepts (from the rich conceptual vocabulary of his works). the image/idea of deterritorialization especially has come to enter into and be re-appropriated by other domains and disciplines (most notably anthropology), so it may be useful to have an accurate sense/understanding of what it actually implies (given the unusual, weird and slippery nature of deleuzian concepts).

![](https://i.imgur.com/cG3Ei3o.jpg)

*a sketch/diagram of the categories of assemblages and the directions in which different forces pull. regimes of signs are considered to trigger re-territorialization and seize/capture/prevent lines of flight (deterritorializations) — one example of this would be religion (the [moralizing, monotheistic, large-scale-soceties kind](http://peterturchin.com/cliodynamica/the-transcendental-revolution/) of religions). source: [theoryandeducation.wordpress.com](https://theoryandeducation.wordpress.com)*

------

there's a distinction drawn between **relative** and **absolute** deterritorialization in [*a thousand plateaus*](http://gen.lib.rus.ec/search.php?req=thousand+plateaus&lg_topic=libgen&open=0&view=simple&res=25&phrase=1&column=def) (ATP) — relative deterritorialization is always transitory and accompanied by **re-territorialization**, while positive/absolute deterritorialization triggers/enacts something close to the construction of a ["plane of immanence"](https://en.wikipedia.org/wiki/Plane_of_immanence) (spinoza's ontological constitution of the world as an example of which).

> *Qualitatively speaking there are two different deterritorializing movements: absolute and relative. Philosophy is an example of absolute deterritorialization and capital is an example of relative deterritorialization. Absolute deterritorialization is a way of moving and as such it has*
> *nothing to do with how fast or slow deterritorializing movements are; such movements are immanent, differentiated and ontologically prior to the movements of relative deterritorialization. Relative deterritorialization moves towards fixity and as such it occurs not on a molecular but molar plane as an actual movement. Put succinctly, absolute deterritorializing movements are virtual, moving through relative deterritorializing movements that are actual.*
>
> ​               — Adrian Parr, "The Deleuze Dictionary" (Edinburgh University Press 2010)

the function of deterritorialization is defined as **"the movement by which one leaves a territory"**, also referred to as a **"line of flight"** — still, deterritorialization **"constitutes and extends"** the territory itself. depending on the context within which it is used (and in relation to what), as transformative vectors deterritorializations tend to indicate the creative potential of an assemblage, by freeing it up from the fixed relations that contain/constrain a body, and in exposing it to new organizations and adaptable (networked) arrangements.

[levi r. bryant](https://en.wikipedia.org/wiki/Levi_Bryant) (object-oriented ontologist that focused a lot on deleuze) has an [explanatory blog post on deterritorialization](https://larvalsubjects.wordpress.com/2011/07/02/deterritorialization/) at his site ([larvalsubjects](https://larvalsubjects.wordpress.com/)):

> *For me, the concept was really driven home when, somewhere in A Thousand Plateaus, I came across Deleuze and Guattari’s remark that **“a club is a deterritorialized branch.”** The territory of a branch, is, of course, a tree. The branch serves the function of extending leaves across an area so as to capture sunlight. Perhaps **the best definition of deterritorialization is the decontextualization of something or a theft of a bit of code that then resituates that thing elsewhere**. Here “code” is to be understood as formed matter that serves a particular function. When code is stolen it is separated and isolated from its original milieu or territory, liberated from its original function, and then resituated in a new territory. When the branch is separated from the tree it becomes something else, it takes on different functions, such that it has been deterritorialized from its original territory (the function of gathering sunlight in the process of photosynthesis) and reterritorialized elsewhere (the function of warfare or violence). Deterritorialization thus proceeds through subtraction. As Deleuze and Guattari remark in their famous rhizome essay, deterritorialization “…begins by selecting or isolating…”* 
>
> *[...]*
>
> _**Codes are always functional.** The tree bark serves a particular function for the tree, the greenness of leaves serves a particular function for leaves or is a bi-product of functions like photosynthesis. In stealing a bit of code, quality is divorced from function and takes on a new function for these animals. **There is not representation, resemblance, or imitation, but rather the formation of a new set of functions.**_

so, deterritorialization can also be said to constitute a two-step operation of appropriation and re-purposing, whether deliberate or accidental — which is also a well-studied evolutionary phenomenon (of a creative accident radically changing the course of events).