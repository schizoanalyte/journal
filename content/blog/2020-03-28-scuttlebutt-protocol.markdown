---
layout: post
title:  "scuttlebutt: gossip protocol for decentralized p2p apps <-- perhaps just what we've/i've been looking for!"
date:   2020-03-28 01:59:14 +0200

---
![](https://opencollective-production.s3-us-west-1.amazonaws.com/019cedd0-0249-11e8-90df-3322b481b909.png)

> ##### **scuttlebutt (n.)**
>
> - *gossip or rumor. a scuttlebutt was formerly the cask on a ship that held the day's supply of drinking water and a common place for sailors to engage in gossip.*

<b> <b>

<b> <b>

**scuttlebutt** is a **p2p gossip protocol for decentralized apps** (created by [dominic tarr](https://twitter.com/dominictarr)) that is very minimal and also designed having off-grid living in mind (i.e., that it can be used offline). scuttlebutt is flexible and may generalize across many different types of apps that it makes technically possible and implementable (here's an [example of a tiny chat-like cli built on scuttlebutt](https://github.com/stripedpajamas/scat)), but the first app which was built on the basis of the scutterbutt protocol was a **social network**. 

<b> <b>

one that is completely decentralized and p2p (not federated, like [mastodon](https://mastodon.social/) or [diaspora](https://diasporafoundation.org)), running as a stand-alone app (coming in two flavors: **patchwork** and **patchbay**) and designed as a kind of personal diary with twitter-like interface/feeds (but no characters/word limits, etc.) and support for markdown. it allows you to use/connect to a scuttlebutt server (called a [**"pub"**](https://ssbc.github.io/scuttlebutt-protocol-guide/#pubs)) in order to synchronize and update your client with the latest current state of things (posts, events, peers to connect to, etc. — all of which you can also access, sift through and read offline).

<b> <b>

user feeds in the scuttlebutt social network are distributed across a [**DHT**](https://en.wikipedia.org/wiki/Distributed_hash_table) and a hashchain is used for users' names registry. the network topology is such that it maps the various links and connections directly, i.e. if you follow somebody, you are actually following them at the data layer without any intermediaries or servers.

<br>

each unique scuttlebutt id is associated to its individual feed, which is an append-only hashchain (similarly to holochain) — so, in blockchain terms/vernacular, scuttlebutt is basically a private-permissioned database operating on proof-of-stake where there's only one staker per blockchain, but there's unlimited many chains all over the place and these chains interact (transact, share, broadcast, etc.) in such ways that it makes them tangled together. 

in technical terms, it's really all just signatures and hashes.

also, you only replicate what is relevant to you. in scuttlebutt you just have an offline first view of the data that you have and if someone from your secure neighborhood goes on to visit some friends of yours or whatever, this may replicate your data to them so they have a copy. this is not too fast, of course, but it works (under conditions of necessity).

again, very reminiscent of holochain's architecture and design rationale in many ways.

<br>

there are 3 implementations of scuttlebutt:

- a **javascript** one (node.js) — the reference implementation
- a **go** one (which is usable, but not complete)
- and a **rust** one being underway

<br>

there's also a [bundle of popular apps built on top of scuttlebutt available](https://github.com/ssbc/handbook.scuttlebutt.nz/blob/master/applications.md), like chess, git, etc. the one that caught my attention was [ticktack](https://github.com/ticktackim/ticktack-workplan/releases) —  a blog publishing app for long-form articles (coming with integrated private messaging).

scuttlebutt's development philosophy is to go directly for use value, i.e. whatever it is you're building or putting together, it must be guided by the clear purpose of its immediate utility.

<br>

the whole thing seems to be very much along the same lines of what i've been thinking about and having in mind for some time now. some kind of customizable open source social networking protocol that can be implemented and deployed in all sorts of ways, is extensible/modular and relatively simple to build from.

<br>

scutterbutt seems very much in the vein of internet protocols, services and apps from that nostalgic bygone era before the corporate capture of what we now call [web 2.0](https://en.wikipedia.org/wiki/Web_2.0), when protocols like [usenet](https://en.wikipedia.org/wiki/Usenet) and [irc](https://en.wikipedia.org/wiki/Internet_Relay_Chat) used to define the internet.

<br>

also, in many ways scutterbutt shares some (both technically and conceptually) common ground with [holochain](https://holochain.org), but scutterbutt seems simpler and more practical, while [holochain's ambitious vision](http://ceptr.org) goes way beyond that simple utility.

<br>

#### here's some useful resources:

<br>

- [ssb handbook](https://github.com/ssbc/handbook.scuttlebutt.nz) — a guide to the secure scuttlebutt key concepts & influences
- [scalable secure scuttlebutt](https://github.com/dominictarr/scalable-secure-scuttlebutt/blob/master/paper.md) — notes on a paper for ssb
- ["Designing a Secret Handshake: Authenticated Key Exchange as a Capability System"](http://dominictarr.github.io/secret-handshake-paper/shs.pdf) (pdf)
- [a collection of links to known resources](https://github.com/ssbc/scuttlebutt-guide)
- [a field guide to developing with secure-scuttlebutt](https://github.com/nichoth/ssb-field-guide)
- [ssb-npm-101](https://github.com/noffle/ssb-npm-101) — like **npm**, except the registry of packages (metadata + tarballs) resides entirely on secure scuttlebutt (installed [scuttlebot](https://github.com/ssbc/ssb-server) instance is a prerequisite)
- [scuttlebot](https://github.com/ssbc/scuttlebot) (sbot) — provides network services on top of secure-scuttlebutt (the database); scuttlebot is the gossip and replication server for secure scuttlebutt (a distributed social network)
  - see also: [epidemic broadcast trees (eli5)](https://github.com/nichoth/ssb-field-guide/blob/master/gossip.md) 
- [ssb](https://github.com/ssbc/secure-scuttlebutt) (database layer for secure-scuttlebutt) — a database of unforgeable append-only feeds, optimized for efficient replication for peer-to-peer protocols, implemented with [flumedb](https://github.com/flumedb/flumedb) (feeds are  signed append-only sequences of messages)
- [scuttletron](https://github.com/KGibb8/scuttletron) — an electron app with integrated scuttlebot server for quick-start application development
- [an intro to scuttlebutt development: spinning up a test environment](https://josiahwitt.com/2018/07/08/scuttlebutt-intro-test-playground.html) 
- [an intro to scuttlebutt development: flume & plugins](https://josiahwitt.com/2018/07/08/scuttlebutt-intro-flume.html)

